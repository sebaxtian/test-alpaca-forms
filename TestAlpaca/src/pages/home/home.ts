import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

/* Para incluir Librerias externas Via HTML <script>
 * y usar desde TypeScript en Ionic 2 sin problema, por ejemplo jQuery.
 * 
 * Ver: Script Tag Include
 * [https://ionicframework.com/docs/v2/resources/app-scripts/]
 */
declare const jQuery:any;


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    jQuery(document).ready(function() {
      
      console.log("El DOM esta Listo !!");

      //	Alpaca.logLevel = Alpaca.DEBUG;

          /**
           * Initial data
           *
           * Fill in the JSON data that should be populated into the form.  This can be any JSON data that you'd like
           * provided that it fits the schema and options (if you provide those).
           *
           */
          var data = {
              "name": "Inigo Montoya",
              "age": 29,
              "phone": "414-111-2222",
              "country": "usa"
          };

          /**
           * JSON-schema for the form
           *
           * The form schema defines the data types, validation logic and other constraints that need to be satisfied in
           * order for the form to be considered valid.
           *
           * This should follow the JSON-schema convention.
           * @see http://json-schema.org
           *
           * Full schema settings are listed here:
           * @see http://www.alpacajs.org
           *
           */
          var schema = {
      "title":"alpaca prueba",
      "type":"object",
      "properties":{
         "prueba_1":{
            "type":"string",
            "title":"prueba 1",
            "placeholder":"escribe tu texto corot",
            "required":true,
            "autocompletar":""
         },
         "texto_largo_1":{
            "type":"string",
            "title":"Texto Largo 1",
            "placeholder":"placeholder de textarea",
            "required":true,
            "autocompletar":""
         },
         "numero_2":{
            "type":"string",
            "title":"Numero 2",
            "placeholder":"placeholder numero",
            "required":true,
            "autocompletar":""
         },
         "fecha_3":{
            "type":"string",
            "title":"Fecha 3",
            "placeholder":"dd/mm/aaaa",
            "autocompletar":null
         },
         "email_4":{
            "type":"string",
            "title":"Email 4",
            "placeholder":"usuario@mail.com",
            "autocompletar":""
         },
         "_seleccion_multiple_5":{
            "type":"string",
            "title":" Seleccion multiple 5",
            "placeholder":null,
            "enum":[
               "checkbox-5-1",
               "checkbox-5-2",
               "cheackbox 3"
            ],
            "autocompletar":null
         },
         "_seleccion_unica_6":{
            "type":"string",
            "title":" Seleccion Unica 6",
            "placeholder":null,
            "enum":[
               "radio-6-si",
               "radio-6-no"
            ],
            "required":true,
            "autocompletar":null
         },
         "_menu":{
            "type":"string",
            "title":" Menu",
            "placeholder":null,
            "enum":[
               "seleccione",
               "trews",
               "Nueva cuatro"
            ],
            "autocompletar":null
         }
      }
   };

          /**
           * Layout options for the form
           *
           * These options describe UI configuration for the controls that are rendered for the data and schema.  You can
           * tweak the layout and presentation aspects of the form in this section.
           *
           * Full options settings are listed here:
           * @see http://www.alpacajs.org
           *
           */
          var options = {
      "fields":{
         "prueba_1":{
            "type":"text",
            "placeholder":"escribe tu texto corot",
            "helper":"ayuda de texto coroto"
         },
         "texto_largo_1":{
            "type":"textarea",
            "placeholder":"placeholder de textarea",
            "helper":"ayuda de textarea"
         },
         "numero_2":{
            "type":"number",
            "placeholder":"placeholder numero",
            "helper":"ayuda de numero"
         },
         "fecha_3":{
            "type":"date",
            "placeholder":"dd/mm/aaaa",
            "helper":"ayuda fecha"
         },
         "email_4":{
            "type":"email",
            "placeholder":"usuario@mail.com",
            "helper":"correo"
         },
         "_seleccion_multiple_5":{
            "type":"checkbox",
            "placeholder":null,
            "optionLabels":[
               "checkbox-5-1",
               "checkbox-5-2",
               "cheackbox 3"
            ],
            "helper":"ayuda de chackbox"
         },
         "_seleccion_unica_6":{
            "type":"radio",
            "placeholder":null,
            "optionLabels":[
               "radio-6-si",
               "radio-6-no"
            ],
            "helper":"ayuda de radio"
         },
         "_menu":{
            "type":"select",
            "placeholder":null,
            "optionLabels":[
               "seleccione",
               "trews",
               "Nueva cuatro"
            ],
            "helper":"ayuda de select"
         }
      }
   };

          /**
           * This is an optional post render callback that Alpaca will call once the form finishes rendering.  The form
           * rendering itself is asynchronous as it may load templates or other resources for use in generating the UI.
           *
           * Once the render is completed, this callback is fired and the top-level Alpaca control is handed back.
           *
           * @param control
           */
          var postRenderCallback = function(control) {

          };

          /**
           * Render the form.
           *
           * We call alpaca() with the data, schema and options to tell Alpaca to render into the selected dom element(s).
           */
          jQuery("#form").alpaca({
              "data": data,
              "schema": schema,
              "options": options,
              "postRender": postRenderCallback,
              //"view": "bootstrap-edit"//,
              "view": "bootstrap-edit-horizontal"
          });
      });
  }

}
